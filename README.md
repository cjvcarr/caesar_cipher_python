# README #

This is a README for my project, caesar_cipher_python (caesar.py)

### What is this repository for? ###

* This a quick Caesar and Vignere cipher encoder/decoder written in python


### How do I get set up? ###

You can run the script using the following commands: 

usage: caesar.py [-h] -m MESSAGE -k KEY [-d] [-e]

optional arguments:
  -h, --help    show this help message and exit
  -m MESSAGE    enter a string to be decoded or encoded
  -k KEY        enter the key or keyword (lower case alphabetical) to use for
                encoding/decoding
  -d, --decode  decode the given cipher
  -e, --encode  encode the given string

Tests can be run using the test_caesar.py script

Created by Christopher Carr, ITCollege C11