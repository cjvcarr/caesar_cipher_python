import argparse
import string

ALPHABET_LOWER = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

def main():
	
	args = init_arg_parser()
	if args.decode:
		decode(args.message, args.key)
	if args.encode:
		encode(args.message, args.key)

def init_arg_parser():

	parser = argparse.ArgumentParser()
	parser.add_argument('-m', metavar='MESSAGE', dest='message', required=True, help='enter a string to be decoded or encoded')
	parser.add_argument('-k', metavar='KEY', dest='key', required = True, help='enter the key or keyword (lower case alphabetical) to use for encoding/decoding')
	parser.add_argument('-d', '--decode', dest='decode', action='store_true', help='decode the given cipher')
	parser.add_argument('-e', '--encode', dest='encode', action='store_true', help='encode the given string')
	return parser.parse_args()
	
def encode(message, key):
	ordmessage = ordlist(message)
	keynum = []
	encoded_message = []
	if key != None:
		keyword_ord = keyword_to_ord(key)
		for i in range(len(keyword_ord)):
			keynum.append(keyword_ord[i])
	else:
		print('Error - incorrect key entered!')
		return
	j = 0
	for i in range(len(ordmessage)):
		current_char = ordmessage[i]
		if (current_char >= 97 and current_char <= 122): 
			if current_char + keynum[j] > 122:
				encoded_message.append(chr(current_char-(26-keynum[j])))
			else:				
				encoded_message.append(chr(current_char+keynum[j]))
		elif (current_char >=65 and current_char <= 90):
			if current_char + keynum[j] > 90:
				encoded_message.append(chr(current_char-(26-keynum[j])))
			else:				
				encoded_message.append(chr(current_char+keynum[j]))	
		else:
			encoded_message.append(chr(current_char))
			continue
		if j == len(keynum)-1:
			j = 0
		else:
			j += 1
		
	encoded_string = ''.join(encoded_message)
	print(encoded_string)
	return(encoded_string)
	
	
def decode(message, key):
	ordmessage = ordlist(message)
	keynum = []
	decoded_message = []
	if key != None:
		keyword_ord = keyword_to_ord(key) #EDIT THIS PART!
		for i in range(len(keyword_ord)):
			keynum.append(keyword_ord[i])
	else:
		print('Error - incorrect key entered!')
		return
	j = 0
	for i in range(len(ordmessage)):
		current_char = ordmessage[i]
		if (current_char >= 97 and current_char <= 122): 
			if current_char - keynum[j] < 97:
				decoded_message.append(chr(current_char+(26-keynum[j])))
			else:				
				decoded_message.append(chr(current_char-keynum[j]))
		elif (current_char >=65 and current_char <= 90):
			if current_char - keynum[j] < 65:
				decoded_message.append(chr(current_char+(26-keynum[j])))
			else:				
				decoded_message.append(chr(current_char-keynum[j]))	
		else:
			decoded_message.append(chr(current_char))
			continue
		if j == len(keynum)-1:
			j = 0
		else:
			j += 1

	decoded_string = ''.join(decoded_message)
	print(decoded_string)
	return(decoded_string)

def ordlist(message):
	message_list = list(message)
	for i in range(len(message_list)):
		message_list[i] = ord(message_list[i])
	return message_list

def keyword_to_ord(keyword):
	keylist = list(keyword)
	for i in range(len(keylist)):
		if keylist[i] in ALPHABET_LOWER: 
			keylist[i] = ALPHABET_LOWER.index(keylist[i])
		else:
			return None
	return keylist

if __name__ == '__main__':
	main()
