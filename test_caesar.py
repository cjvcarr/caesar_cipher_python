import unittest
from caesar import encode
from caesar import decode

class CaesarTests (unittest.TestCase):
	def test_encodes_correctly_with_key(self):
		encoded_message = encode('Message for testing', 'k')	
		self.assertEqual(encoded_message, 'Wocckqo pyb docdsxq')
	
	def test_encodes_correctly_with_keyword(self):
		encoded_message = encode('Message for testing', 'key')
		self.assertEqual(encoded_message, 'Wiqceeo jmb xccxgxk')
		
	def test_decodes_correctly_with_key(self):
		decoded_message = decode('Wocckqo pyb docdsxq', 'k')
		self.assertEqual(decoded_message, 'Message for testing')
		
	def test_decodes_correctly_with_keyword(self):
		decoded_message = decode('Wiqceeo jmb xccxgxk', 'key')
		self.assertEqual(decoded_message, 'Message for testing')

if __name__ == "__main__":
	unittest.main()

